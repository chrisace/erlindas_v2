// Intersection Observer
//////////////////////////////
const options = {
  root: document.querySelector(".scrollingDiv"),
  rootMargin: "300px",
  threshold: [0, 0.5, 1.0]
};

const callback = (entries, observer) => {
  entries.forEach(entry => {
    // console.log(entry)
    if (entry.isIntersecting && entry.intersectionRatio >= 0.15) {
      entry.target.classList.add("visible");
      if (entry.target.classList.contains("lazy")) {
        if (entry.target.classList.contains("lazy--pic")) {
          const sources = entry.target.querySelectorAll("source");
          sources.forEach(function(i) {
            const src = i.dataset.srcset;
            i.srcset = src;
          });
        } else {
          const src = entry.target.dataset.src;
          entry.target.src = src;
        }
      }

      observer.unobserve(entry.target);
    }
  });
};

const observer = new IntersectionObserver(callback, options);

const boxes = document.querySelectorAll("section, article, .lazy");
boxes.forEach(box => observer.observe(box));

/*
// Get body Height
//////////////////////////////

let timeout = false // holder for timeout id
const intro = document.querySelector('.intro');
const body = document.querySelector('body');

// window.resize event listener
window.addEventListener('resize', function () {
  // clear the timeout
  clearTimeout(timeout);
  // start timing for event "completion"
  timeout = setTimeout(() => {
    console.log('the intro height is ' + intro.offsetHeight)
    console.log('the body height is ' + body.offsetHeight)
    intro.style.height = body.offsetHeight + 'px';
  }, 250);
});

intro.style.height = body.offsetHeight+'px';

*/

let timeout = false; // holder for timeout id
const intro = document.querySelector(".intro");
const body = document.querySelector("body");
const main = document.querySelector("main");

// window.resize event listener
window.addEventListener("resize", function() {
  // clear the timeout
  clearTimeout(timeout);
  // start timing for event "completion"
  timeout = setTimeout(() => {
    main.style.marginTop = intro.offsetHeight + "px";
  }, 250);
});

main.style.marginTop = intro.offsetHeight + "px";
